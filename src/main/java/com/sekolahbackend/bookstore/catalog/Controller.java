package com.sekolahbackend.bookstore.catalog;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("api")
    public String api(){
        return "hello world";
    }

}
